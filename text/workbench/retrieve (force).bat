"%~dp0\bin\tx.exe" pull -a -f --mode=reviewed
"%~dp0\bin\fnr.exe" --cl --find "^# " --replace "" --fileMask "*.*" --dir "%~dp0\fr" --excludeFileMask "*.exe, *.dll" --includeFilesWithoutMatches --useRegEx --includeSubDirectories
"%~dp0\bin\fnr.exe" --cl --find "&quot;" --replace "\"" --fileMask "*.*" --dir "%~dp0\fr" --excludeFileMask "*.exe, *.dll" --includeFilesWithoutMatches --useRegEx --includeSubDirectories
"%~dp0\bin\fnr.exe" --cl --find "\\\\" --replace "\\" --fileMask "*.*" --dir "%~dp0\fr" --excludeFileMask "*.exe, *.dll" --includeFilesWithoutMatches --useRegEx --includeSubDirectories
