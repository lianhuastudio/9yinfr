OutFile "Install FR Language.exe"
RequestExecutionLevel admin

Section

IfFileExists "fxlaunch.exe" +3 0
MessageBox MB_OK "Veuillez mettre ce programme dans le dossier du jeu a cote du ficher 'fxlaunch'"
Quit

inetc::get /USERAGENT curl/1.0 https://sourceforge.net/projects/jiuyinfr/files/files/text.package/download text.package
Delete "$EXEDIR\res\text.package"
Rename $EXEDIR\text.package $EXEDIR\res\text.package

inetc::get /USERAGENT curl/1.0 https://sourceforge.net/projects/jiuyinfr/files/files/gui.package/download gui.package
Delete "$EXEDIR\res\gui.package"
Rename $EXEDIR\gui.package $EXEDIR\res\gui.package

inetc::get /USERAGENT curl/1.0 https://sourceforge.net/projects/jiuyinfr/files/files/gui_language.package/download gui_language.package
Delete "$EXEDIR\res\gui_language.package"
Rename $EXEDIR\gui_language.package $EXEDIR\res\gui_language.package

inetc::get /USERAGENT curl/1.0 https://sourceforge.net/projects/jiuyinfr/files/files/gui_map.package/download gui_map.package
Delete "$EXEDIR\res\gui_map.package"
Rename $EXEDIR\gui_map.package $EXEDIR\res\gui_map.package

inetc::get /USERAGENT curl/1.0 https://sourceforge.net/projects/jiuyinfr/files/files/gui_special.package/download gui_special.package
Delete "$EXEDIR\res\gui_special.package"
Rename $EXEDIR\gui_special.package $EXEDIR\res\gui_special.package

inetc::get /USERAGENT curl/1.0 https://sourceforge.net/projects/jiuyinfr/files/files/skin.package/download skin.package
Delete "$EXEDIR\res\skin.package"
Rename $EXEDIR\skin.package $EXEDIR\res\skin.package

SectionEnd
