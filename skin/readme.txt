skin 界面目录结构
|
|-form_stage_login 	界面目录 登陆环节
|
|-form_stage_create 	界面目录 创建人物环节
|
|-form_stage_roles 	界面目录 选人环节
|
|-form_stage_main	界面目录 游戏内界面
|
|-form_***.xml 通用功能界面
|
|-effect_editor 界面目录 游戏内特效编辑查看器
|
|-move_test	界面目录 游戏内移动和动作测试界面
|
|-story_editor  界面目录 剧情编辑器目录
|
|-resource 界面资源配置目录
	|
	|-HyperLinkStyle.xml 超链接样式配置文件
	|
	|-ImageAnimations.xml 动画图元配置文件
	|
	|-resource.xml 以下资源配置文件
	|
	|-res_cursor.xml 鼠标资源配置文件
	|
	|-res_font.xml 字体资源配置文件
	|
	|-res_sound.xml 界面音效配置文件
